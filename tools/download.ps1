# scarica un file e ne crea il link se viene passato il parametro shortcut
# usage dl -url http://www.pentaonline.it/psc.exe -dest c:\downloads -menushortcut "Teleassistenza Penta srl" -desktopshortcut "Teleassistenza PSC"
param([String] $url, [String] $dest, [String] $menushortcut = '', [String] $desktopshortcut = '')

$webclient = New-Object System.Net.WebClient



$webclient.DownloadFile($url, $dest)

if ($menushortcut -or $desktopshortcut) {
	
	if ($menushortcut){	
		$shortcutname = "C:\Users\All Users\Menu Avvio\Programmi\" + $menushortcut + ".lnk"

		$objShell = New-Object -ComObject ("WScript.Shell")

		$objShortCut = $objShell.CreateShortcut($shortcutname)
		$objShortCut.TargetPath = $dest

		$objShortCut.Save()
	}

	if ($desktopshortcut){
		$shortcutname = $env:PUBLIC + "\Desktop\" + $desktopshortcut + ".lnk"

		$objShell = New-Object -ComObject ("WScript.Shell")

		$objShortCut = $objShell.CreateShortcut($shortcutname)
		$objShortCut.TargetPath = $dest

		$objShortCut.Save()

	}
	
}

