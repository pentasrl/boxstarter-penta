$install_dir = "c:\install"
if((Test-Path $install_dir) -eq 0) { mkdir $install_dir }

Import-Module Boxstarter.Chocolatey

Set-WindowsExplorerOptions -DisableShowHiddenFilesFoldersDrives -DisableShowProtectedOSFiles -EnableShowFileExtensions

# Aggiorna versione Powershell
cinst -y powershell
# Aggiorna versione chocolatey
cinst -y chocolatey

# Scarica Teleassistenza TeamViewer - Penta e crea collegamento su start
.\tools\download -url "http://files.pentaonline.it/penta_qs-idcznfs2vx.exe" -dest "$install_dir\penta_qs-idcznfs2vx.exe" -menushortcut "Teleassistenza Penta srl" -desktopshortcut "Teleassistenza Penta srl"

# Scarica il setup di Webroot
.\tools\download -url "http://files.pentaonline.it/SAE6WRSM598822EFDEF4.exe" -dest "$install_dir\SAE6WRSM598822EFDEF4.exe" 

# Scarica il setup di Kaseya per le workstation
.\tools\download -url "http://files.pentaonline.it/kaseya-ws.exe" -dest "$install_dir\kaseya-ws.exe" 

# Installazione dei software
# https://chocolatey.org/packages/notepadplusplus
cinst -y notepadplusplus
# https://chocolatey.org/packages/google-chrome-x64
cinst -y google-chrome-x64
# https://chocolatey.org/packages/javaruntime
cinst -y javaruntime
# https://chocolatey.org/packages/Firefox
cinst -y firefox
#https://chocolatey.org/packages/thunderbird
cinst -y thunderbird
# https://chocolatey.org/packages/adobereader
cinst -y adobereader
# https://chocolatey.org/packages/PDFCreator
cinst -y PDFCreator
# https://chocolatey.org/packages/sysinternals
cinst -y sysinternals
