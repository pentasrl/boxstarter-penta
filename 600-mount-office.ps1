$install_dir = "c:\install"

Import-Module Boxstarter.Chocolatey

# https://chocolatey.org/packages/imdisk
cinst -y imdisk

imdisk -d -m  "w:"

# Next, mount the ISO file, ready for using it's contents (NOTE: the last parameter here is the drive letter that will be assigned to the mounted ISO)
# imdisk -a -f "$install_dir\office_2013_pkc.iso" -m "w:"
imdisk -a -f "$install_dir\office_2016_pkc.iso" -m "w:"

w:
.\setup.exe

# Unmount the ISO file when finished
# imdisk -d -m w: