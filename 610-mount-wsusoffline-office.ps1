$install_dir = "c:\install"

Import-Module Boxstarter.Chocolatey

# https://chocolatey.org/packages/imdisk
cinst -y imdisk

# Next, mount the ISO file, ready for using it's contents (NOTE: the last parameter here is the drive letter that will be assigned to the mounted ISO)
imdisk -a -f "$install_dir\wsusoffline-ofc-ita.iso" -m "w:"

w:\UpdateInstaller.exe

# Unmount the ISO file when finished
# imdisk -d -m w: