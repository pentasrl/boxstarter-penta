# https://github.com/chocolatey/choco/wiki/How-To-Mount-An-Iso-In-Chocolatey-Package

$install_dir = "c:\install"
$iso_source = "\\192.168.71.8\public"

Import-Module Boxstarter.Chocolatey

if((Test-Path $install_dir) -eq 0) { mkdir $install_dir }

net use N: $iso_source /USER:deploy

# Copia i file ISO del wsus dal NAS
# windows 7 64 bit
Copy-Item N:\wsus_iso\wsusoffline-w61-x64.iso $install_dir 
# office 2010/2013
# Copy-Item N:\wsus_iso\wsusoffline-ofc-ita.iso $install_dir 

# Copia l'ISO per il setup di office 2013
# Copy-Item "N:\Microsoft MAPS\Office PKC\office_2013_pkc.iso" $install_dir

# Copia l'ISO per il setup di office 2013
Copy-Item "N:\Microsoft MAPS\Office PKC\office_2016_pkc.iso" $install_dir

net use N: /DELETE
