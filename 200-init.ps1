Import-Module Boxstarter.Chocolatey

$install_dir = "c:\install"
if((Test-Path $install_dir) -eq 0) { mkdir $install_dir }

$install_totcmd_dir = "c:\totalcmd"

Import-Module Boxstarter.Chocolatey
if((Test-Path $install_totcmd_dir) -eq 0) { mkdir $install_totcmd_dir }

# Scarica TotCMD e salva su Install
.\tools\download -url "http://files.pentaonline.it/totalcmd.exe" -dest "$install_dir\totalcmd.exe"
c:
cd $install_dir
.\totalcmd.exe c:\

#Scarica la licenza
cd $install_dir\boxstarter-penta\
.\tools\download -url "http://files.pentaonline.it/wincmd.key" -dest "$install_totcmd_dir\wincmd.key"

