# By default, PowerShell blocks the running of scripts and will therefore block the execution of all
# Boxstarter commands. If you have not done so already on the machine you are using Boxstarter, there is a one time
# command you must run in order to lift this restriction
# Set-ExecutionPolicy Unrestricted -Force

iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))
cup chocolatey
cinst -y boxstarter
